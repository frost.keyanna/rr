package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class SurpriseActivity extends Activity {

    Random r = new Random();

    String[] items = new String[] {
            "BAKED LEMON CHICKEN\n\n" +
                    "INGREDIENTS\n" +
                    "4 boneless skinless chicken breasts\n" +
                    "3 tablespoons butter\n" +
                    "1/3 cup chicken broth\n" +
                    "4 tablespoons fresh lemon juice\n" +
                    "1 tablespoon honey\n" +
                    "2 teaspoons minced garlic\n" +
                    "1 teaspoon Italian seasoning\n" +
                    "1 salt and pepper to taste\n" +
                    "optional: fresh rosemary and lemon slices for garnish\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 400 degrees and grease a baking sheet or large casserole dish.\n" +
                    "Melt butter in a large skillet over medium-high heat. Add chicken and cook chicken 2-3 minutes on each side just until browned. Transfer chicken to prepared baking sheet.\n" +
                    "In a small bowl whisk together chicken broth, lemon juice, honey, garlic, Italian seasoning, and salt and pepper.\n" +
                    "Pour sauce over chicken. Bake 20-30 minutes (closer to 20 for smaller chicken breasts, closer to 30 for larger) until chicken is cooked through. Every 5-10 minutes spoon the sauce from the pan over the chicken.\n" +
                    "Garnish with fresh rosemary and lemon slices if desired and serve.",

            "HONEY GARLIC CHICKEN\n\n" +
                    "INGREDIENTS\n" +
                    "6 chicken thighs, bone in or out with or without skin\n" +
                    "Salt and pepper, to season\n" +
                    "2 teaspoons garlic powder, to season\n" +
                    "6 cloves garlic, crushed\n" +
                    "1/3 cup honey\n" +
                    "1/4 cup water (or chicken broth)\n" +
                    "2 tablespoons vinegar\n" +
                    "1 tablespoon soy sauce\n\n" +
                    "INSTRUCTIONS\n" +
                    "Season chicken with salt, pepper and garlic powder; set aside.\n" +
                    "Heat a pan or skillet over medium high heat; sear chicken thigh fillets or breast fillets on both sides until golden and cooked through.\n\n" +
                    "FOR BONE IN THIGHS:\n" +
                    "Reduce heat after searing on both sides, cover skillet with a lid and continue cooking until the chicken is cooked through, while turning every 5 minutes until done. Alternatively, see notes for oven method.\n" +
                    "Drain most of the excess oil from the pan, leaving about 2 tablespoons of pan juiced for added flavour.\n\n" +
                    "FOR SAUCE:\n" +
                    "When chicken is done and cooked through, arrange chicken skin-side up in the pan (if cooking with skin); add the garlic between the chicken and fry until fragrant (about 30 seconds). Add the honey, water, vinegar and soy sauce. Increase heat to medium-high and continue to cook until the sauce reduces down and thicken slightly (about 3-4 minutes).\n" +
                    "Garnish with parsley and serve over vegetables, rice, pasta or with a salad.",

            "CHICKEN PICCATA\n\n" +
                    "INGREDIENTS\n" +
                    "4 chicken breasts\n" +
                    "1/2 cup flour\n" +
                    "3 TB extra virgin olive oil\n" +
                    "1/2 cup onion, chopped\n" +
                    "8 cloves garlic, minced\n" +
                    "2 TB freshly squeezed lemon juice (about 1 large lemon)\n" +
                    "1 lemon, thinly sliced\n" +
                    "1/2 cup regular chicken broth\n" +
                    "1 TB capers, drained\n" +
                    "1 TB salted butter\n" +
                    "kosher salt + freshly ground black pepper, to taste\n" +
                    "Optional garnish: freshly chopped parsley\n\n" +
                    "INSTRUCTIONS\n" +
                    "Place chicken breasts on cutting board and cover with plastic wrap. Pound to even thickness. Use paper towels to dab off excess moisture from chicken.\n" +
                    "Sprinkle each piece of chicken with a thin layer of kosher salt and freshly ground black pepper on both sides. Place flour in a shallow dish. Coat each piece of chicken in flour, shaking off excess.\n" +
                    "In an extra large skillet, heat 2 TB olive oil over medium high heat until oil is hot. Add chicken to skillet (cook in batches if needed, to avoid overcrowding.) Cook for 2-3 minutes of each side or until no longer pink in center; don’t overcook. Use tongs to transfer chicken to a separate plate; keep warm.\n" +
                    "Without cleaning the skillet out, add 1 TB olive oil to same skillet over medium high heat. When oil is hot, add onion and garlic, stirring 1-2 min until fragrant. Add lemon juice and lemon slices, scraping pan if needed to loosen any browned bits. Cook until lemon slices are nicely browned. Add broth, capers and butter, stirring just until butter melts. Pour lemon mixture over chicken and serve. Garnish with freshly chopped parsley, if desired.",


            "BAKED EGGPLANT PARMESAN\n\n" +
                    "INGREDIENTS\n" +
                    "1 1/2 cup panko bread crumbs\n" +
                    "1 cup freshly grated Parmesan, divided\n" +
                    "2 tsp. Italian seasoning\n" +
                    "Kosher salt\n" +
                    "Freshly ground black pepper\n" +
                    "2 medium eggplants, sliced into 1/2 inch thick rounds\n" +
                    "3 large eggs\n" +
                    "4 cups marinara\n" +
                    "2 cups shredded mozzarella\n" +
                    "1/3 cup thinly sliced basil\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 425°. Line two large baking sheets with parchment paper and " +
                    "coat with cooking spray. In a shallow bowl, whisk together panko, 1/2 cup " +
                    "Parmesan, and Italian seasoning. Season with salt and pepper. In another " +
                    "shallow bowl, whisk eggs with 2 tablespoons water and season with salt and pepper.\n" +
                    "Dip an eggplant slice into egg wash, then dredge in panko mixture. Place on " +
                    "baking sheet. Repeat to coat all eggplant slices. Spray tops lightly with cooking spray.\n" +
                    "Bake until soft inside, and golden and crisp on the outside, 30 to 35 minutes.\n" +
                    "In a large baking dish, add a cup of marinara and spread evenly. Add an even " +
                    "layer of eggplant slices, then pour 1 1/2 cup marinara on top. Sprinkle " +
                    "with 1 cup mozzarella, half of the remaining Parmesan, and fresh basil. " +
                    "Repeat process once more to use up all ingredients.\n" +
                    "Bake until bubbly and golden, 15 to 17 minutes more.",

            "PORTOBELLO MUSHROOM BURGER\n\n" +
                    "INGREDIENTS\n" +
                    "4 large portobello mushrooms, stems and gills removed\n" +
                    "1/3 cup extra-virgin olive oil\n" +
                    "1 tsp. garlic powder\n" +
                    "1/2 tsp. ground mustard\n" +
                    "1 tbsp. Worcestershire sauce\n" +
                    "4 brioche buns\n\n" +
                    "OPTIONAL FOR SERVING:\n" +
                    "Honey mustard\n" + "Tomato slices\n" + "Lettuce\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 375°. In a medium bowl, whisk together olive oil, garlic " +
                    "powder, mustard powder and Worcestershire. Dip mushroom caps in the mixture " +
                    "and place mushrooms face-down on a baking sheet. Sprinkle with salt and " +
                    "pepper, to taste. Bake for 20 minutes, until softened.\n" +
                    "Top with desired toppings and serve.",

            "BEEF AND BROCCOLI\n\n" +
                    "INGREDIENTS\n" +
                    "3 Tablespoons cornstarch, divided\n" +
                    "1 pound flank steak, cut into thin 1-inch pieces\n" +
                    "1/2 cup low sodium soy sauce\n" +
                    "3 Tablespoons packed light brown sugar\n" +
                    "1 Tablespoon minced garlic\n" +
                    "2 teaspoons grated fresh ginger\n" +
                    "2 Tablespoons vegetable oil, divided\n" +
                    "4 cups small broccoli florets\n" +
                    "1/2 cup sliced white onions\n\n" +
                    "INSTRUCTIONS\n" +
                    "In a bowl, mix 2 tablespoons cornstarch with 3 tablespoons water. Add the beef to the bowl and toss to combine.\n" +
                    "\n" +
                    "In a separate bowl, mix the remaining 1 tablespoon cornstarch with the soy sauce, brown sugar, garlic and ginger. Set the sauce aside.\n" +
                    "\n" +
                    "Heat a large sauté pan over medium heat. Add 1 tablespoon of the vegetable oil and once it is hot, add the beef, stirring constantly until the beef is almost cooked through. Using a slotted spoon, transfer the beef to a plate and set it aside.\n" +
                    "\n" +
                    "Add the remaining 1 tablespoon of vegetable oil to the pan and once it is hot, add the broccoli florets and sliced onions and cook, stirring occasionally, until the broccoli is tender, about 4 minutes. (See Notes.)\n" +
                    "\n" +
                    "Return the beef to the pan then add the prepared sauce. Bring the mixture to a boil and cook, stirring, for 1 minute or until the sauce thickens slightly. Serve with rice or noodles.\n" +
                    "\n\n" +
                    "NOTES:\n" +
                    "To guarantee bright green broccoli, blanch the florets in boiling water then drain and dry it very well before adding it to the pan. If you opt for this additional blanching step, reduce the cooking time of the broccoli to 2 minutes.\n" +
                    "\n" +
                    "The sauce must come to a boil in order for the cornstarch to thicken.",

            "Tuna Melt Sandwich\n\n" +
                    "Tuna Fish (Drained from the can)\n" +
                    "Mayonnaise\n" +
                    "Celery, sliced into small pieces.\n" +
                    "Red Onion sliced and diced up. \n" +
                    "Dijon Mustard\n" +
                    "Parsley\n" +
                    "Salt and Pepper\n" +
                    "Bread\n" +
                    "Cheddar Cheese, sliced\n" +
                    "Tomato, sliced thin\n" +
                    "Butter\n\n" +
                    "INSTRUCTIONS\n" +
                    "In a medium sized bowl combine the tuna, mayonnaise, celery, red onion, dijon mustard, parsley, salt and pepper.\n" +
                    "Butter each slice of bread. Putting the butter side down add the cheese on the un-buttered side.\n" +
                    "Add the tuna mixture on top. Top with tomato and additional cheese.\n" +
                    "Add the sandwich on the skillet and heat for about 2-3 minutes until cheese starts to melt and it is golden brown. Flip to the other side and cook until the cheese is fully melted then the sandwich is golden brown.",

            "BAKED SALMON\n\n" +
                    "INGREDIENTS\n" +
                    "2 lemons, thinly sliced\n" +
                    "1 large salmon fillet(about 3 lb.)\n" +
                    "Kosher salt\n" +
                    "Freshly ground black pepper\n" +
                    "6 tbsp. butter, melted\n" +
                    "2 tbsp. honey\n" +
                    "3 cloves garlic, minced\n" +
                    "1 tsp. chopped thyme leaves\n" +
                    "1 tsp. dried oregano\n" +
                    "Chopped fresh parsley, for garnish\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 350°. Line a baking sheet with foil and grease with cooking spray. To the center of the foil, lay lemon slices in an even layer.\n" +
                    "\n" +
                    "Season both sides of the salmon with salt and pepper and place on top of lemon slices.\n" +
                    "\n" +
                    "In a small bowl, whisk together butter, honey, garlic, thyme, and oregano. Pour over salmon then fold up foil around the salmon. Bake until the salmon is cooked through, about 25 minutes. Switch the oven to broil, and broil for 2 minutes, or until the butter mixture has thickened.\n" +
                    "\n" +
                    "Garnish with parsley before serving.",



    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suprise);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items [r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}