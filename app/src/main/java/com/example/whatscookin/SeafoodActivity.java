package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class SeafoodActivity extends Activity {

    Random r = new Random();

    String[] items = new String[]{
            "Tuna Melt Sandwich\n\n" +
                    "Tuna Fish (Drained from the can)\n" +
                    "Mayonnaise\n" +
                    "Celery, sliced into small pieces.\n" +
                    "Red Onion sliced and diced up. \n" +
                    "Dijon Mustard\n" +
                    "Parsley\n" +
                    "Salt and Pepper\n" +
                    "Bread\n" +
                    "Cheddar Cheese, sliced\n" +
                    "Tomato, sliced thin\n" +
                    "Butter\n\n" +
                    "INSTRUCTIONS\n" +
                    "In a medium sized bowl combine the tuna, mayonnaise, celery, red onion, dijon mustard, parsley, salt and pepper.\n" +
                    "\n" +
                    "Butter each slice of bread. Putting the butter side down add the cheese on the un-buttered side.\n" +
                    "\n" +
                    "Add the tuna mixture on top. Top with tomato and additional cheese.\n" +
                    "\n" +
                    "Add the sandwich on the skillet and heat for about 2-3 minutes until cheese starts to melt and it is golden brown. Flip to the other side and cook until the cheese is fully melted then the sandwich is golden brown.",

            "BAKED SALMON\n\n" +
                    "INGREDIENTS\n" +
                    "2 lemons, thinly sliced\n" +
                    "1 large salmon fillet(about 3 lb.)\n" +
                    "Kosher salt\n" +
                    "Freshly ground black pepper\n" +
                    "6 tbsp. butter, melted\n" +
                    "2 tbsp. honey\n" +
                    "3 cloves garlic, minced\n" +
                    "1 tsp. chopped thyme leaves\n" +
                    "1 tsp. dried oregano\n" +
                    "Chopped fresh parsley, for garnish\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 350°. Line a baking sheet with foil and grease with cooking spray. To the center of the foil, lay lemon slices in an even layer.\n" +
                    "\n" +
                    "Season both sides of the salmon with salt and pepper and place on top of lemon slices.\n" +
                    "\n" +
                    "In a small bowl, whisk together butter, honey, garlic, thyme, and oregano. Pour over salmon then fold up foil around the salmon. Bake until the salmon is cooked through, about 25 minutes. Switch the oven to broil, and broil for 2 minutes, or until the butter mixture has thickened.\n" +
                    "\n" +
                    "Garnish with parsley before serving.",

            "GARLIC SHRIMP ALFREDO\n\n" +
                    "INGREDIENTS\n" +
                    "for 4 servings:\n" +
                    "1 lb shrimp, peeled and deveined\n" +
                    "1 teaspoon salt\n" +
                    "½ teaspoon black pepper\n" +
                    "½ medium yellow onion, diced\n" +
                    "1 medium tomato, diced\n" +
                    "3 cloves garlic, minced\n" +
                    "2 cups heavy cream\n" +
                    "1 lb fettuccine, cooked\n" +
                    "1 cup grated parmesan cheese\n\n" +
                    "INSTRUCTIONS\n" +
                    "In a large pot, add the shrimp, salt, and pepper and cook until the shrimp is pink and opaque.\n\n" +
                    "Remove the shrimp from the pot.\n\n" +
                    "Add the onions, tomatoes, and garlic to the pot and cook until the garlic is starting to brown.\n\n" +
                    "Add the cream and bring to a boil.\n\n" +
                    "Once the cream is boiling, add the fettuccine. Return the shrimp to the pan, along with the Parmesan cheese and parsley. Stir until the cheese melts and the sauce coats the shrimp and pasta nicely.",

            "COCONUT SHRIMP\n\n" +
                    "INGREDIENTS\n" +
                    "for 6 servings:\n" +
                    "1 egg\n" +
                    "½ cup all-purpose flour\n" +
                    "⅔ cup beer\n" +
                    "1 ½ teaspoons baking powder\n" +
                    "¼ cup all-purpose flour\n" +
                    "2 cups flaked coconut\n" +
                    "24 medium shrimp\n" +
                    "3 cups oil for frying\n\n" +
                    "INSTRUCTIONS\n" +
                    "In medium bowl, combine egg, 1/2 cup flour, beer and baking powder. Place 1/4 cup flour and coconut in two separate bowls.\n\n" +
                    "Hold shrimp by tail, and dredge in flour, shaking off excess flour. Dip in egg/beer batter; allow excess to drip off. Roll shrimp in coconut, and place on a baking sheet lined with wax paper. Refrigerate for 30 minutes. Meanwhile, heat oil to 350 degrees F (175 degrees C) in a deep-fryer.\n\n" +
                    "Fry shrimp in batches: cook, turning once, for 2 to 3 minutes, or until golden brown. Using tongs, remove shrimp to paper towels to drain. Serve warm with your favorite dipping sauce.",


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seafood);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items[r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
