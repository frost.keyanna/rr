package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class ChickenActivity extends Activity {

    Random r = new Random();

    String[] items = new String[] {
            "BAKED LEMON CHICKEN\n\n" +
                    "INGREDIENTS\n" +
                    "4 boneless skinless chicken breasts\n" +
                    "3 tablespoons butter\n" +
                    "1/3 cup chicken broth\n" +
                    "4 tablespoons fresh lemon juice\n" +
                    "1 tablespoon honey\n" +
                    "2 teaspoons minced garlic\n" +
                    "1 teaspoon Italian seasoning\n" +
                    "1 salt and pepper to taste\n" +
                    "optional: fresh rosemary and lemon slices for garnish\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 400 degrees and grease a baking sheet or large casserole dish.\n" +
                    "Melt butter in a large skillet over medium-high heat. Add chicken and cook chicken 2-3 minutes on each side just until browned. Transfer chicken to prepared baking sheet.\n" +
                    "In a small bowl whisk together chicken broth, lemon juice, honey, garlic, Italian seasoning, and salt and pepper.\n" +
                    "Pour sauce over chicken. Bake 20-30 minutes (closer to 20 for smaller chicken breasts, closer to 30 for larger) until chicken is cooked through. Every 5-10 minutes spoon the sauce from the pan over the chicken.\n" +
                    "Garnish with fresh rosemary and lemon slices if desired and serve.",

            "HONEY GARLIC CHICKEN\n\n" +
                    "INGREDIENTS\n" +
                    "6 chicken thighs, bone in or out with or without skin\n" +
                    "2 teaspoons garlic powder, to season\n" +
                    "6 cloves garlic, crushed\n" +
                    "1/3 cup honey\n" +
                    "1/4 cup water (or chicken broth)\n" +
                    "2 tablespoons vinegar\n" +
                    "1 tablespoon soy sauce\n" +
                    "Salt and pepper, to season\n\n" +
                    "INSTRUCTIONS\n" +
                    "Season chicken with salt, pepper and garlic powder; set aside.\n" +
                    "Heat a pan or skillet over medium high heat; sear chicken thigh fillets or breast fillets on both sides until golden and cooked through.\n\n" +
                    "FOR BONE IN THIGHS:\n" +
                    "Reduce heat after searing on both sides, cover skillet with a lid and continue cooking until the chicken is cooked through, while turning every 5 minutes until done. Alternatively, see notes for oven method.\n" +
                    "Drain most of the excess oil from the pan, leaving about 2 tablespoons of pan juiced for added flavour.\n\n" +
                    "FOR SAUCE:\n" +
                    "When chicken is done and cooked through, arrange chicken skin-side up in the pan (if cooking with skin); add the garlic between the chicken and fry until fragrant (about 30 seconds). Add the honey, water, vinegar and soy sauce. Increase heat to medium-high and continue to cook until the sauce reduces down and thicken slightly (about 3-4 minutes).\n" +
                    "Garnish with parsley and serve over vegetables, rice, pasta or with a salad.",

            "CHICKEN PICCATA\n\n" +
                    "INGREDIENTS\n" +
                    "4 chicken breasts\n" +
                    "1/2 cup flour\n" +
                    "3 TB extra virgin olive oil\n" +
                    "1/2 cup onion, chopped\n" +
                    "8 cloves garlic, minced\n" +
                    "2 TB freshly squeezed lemon juice (about 1 large lemon)\n" +
                    "1 lemon, thinly sliced\n" +
                    "1/2 cup regular chicken broth\n" +
                    "1 TB capers, drained\n" +
                    "1 TB salted butter\n" +
                    "kosher salt + freshly ground black pepper, to taste\n" +
                    "Optional garnish: freshly chopped parsley\n\n" +
                    "INSTRUCTIONS\n" +
                    "Place chicken breasts on cutting board and cover with plastic wrap. Pound to even thickness. Use paper towels to dab off excess moisture from chicken.\n" +
                    "\n" +
                    "Sprinkle each piece of chicken with a thin layer of kosher salt and freshly ground black pepper on both sides. Place flour in a shallow dish. Coat each piece of chicken in flour, shaking off excess.\n" +
                    "\n" +
                    "In an extra large skillet, heat 2 TB olive oil over medium high heat until oil is hot. Add chicken to skillet (cook in batches if needed, to avoid overcrowding.) Cook for 2-3 minutes of each side or until no longer pink in center; don’t overcook. Use tongs to transfer chicken to a separate plate; keep warm.\n" +
                    "\n" +
                    "Without cleaning the skillet out, add 1 TB olive oil to same skillet over medium high heat. When oil is hot, add onion and garlic, stirring 1-2 min until fragrant. Add lemon juice and lemon slices, scraping pan if needed to loosen any browned bits. Cook until lemon slices are nicely browned. Add broth, capers and butter, stirring just until butter melts. Pour lemon mixture over chicken and serve. Garnish with freshly chopped parsley, if desired.",

            "CHICKEN FAJITAS\n\n" +
                    "INGREDIENTS\n" +
                    "6 servings\n" +
                    "1/4 cup lime juice\n" +
                    "1 garlic clove, minced\n" +
                    "1 teaspoon chili powder\n" +
                    "1/2 teaspoon salt\n" +
                    "1/2 teaspoon ground cumin\n" +
                    "2 tablespoons olive oil, divided\n" +
                    "1-1/2 pounds boneless skinless chicken breasts, cut into strips\n" +
                    "1 medium onion, cut into thin wedges\n" +
                    "1/2 medium sweet red pepper, cut into strips\n" +
                    "1/2 medium yellow pepper, cut into strips\n" +
                    "1/2 medium green pepper, cut into strips\n" +
                    "1/2 cup salsa\n" +
                    "12 flour tortillas (8 inches), warmed\n" +
                    "1-1/2 cups shredded cheddar cheese\n\n" +
                    "INSTRUCTIONS\n" +
                    "Mix first 5 ingredients and 1 tablespoon oil. Add chicken; toss to coat. Let stand 15 minutes.\n\n" +
                    "In a large nonstick skillet, heat remaining oil over medium-high heat; saute onion and peppers until crisp-tender, 3-4 minutes. Remove from pan.\n\n" +
                    "In same skillet, saute chicken mixture until no longer pink, 3-4 minutes. Stir in salsa and pepper mixture; heat through.\n\n" +
                    "Serve in tortillas. Sprinkle with cheese."
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chicken);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items [r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}